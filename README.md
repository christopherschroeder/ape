# README #

## Overview ##
APE (A Pipeline for Exome) provides an easy to use way to process exonic data from raw .fastq.gz files to [.bam](https://support.illumina.com/help/BS_App_TruSightTumor15_OLH_1000000001133/Content/Source/Informatics/BAM-Format.htm) and .vcf file for general use and .h5 eagle format files, which are required for APEs parner tool EAGLE. It is based on snakemake and therefore supporting multiprocessing and cluster systems.

## Installation ##

### conda ###

The easiest way to install APE is by using [conda](https://conda.io/docs/) and the bioconda channel:


```
conda install ape -c bioconda

```

### pip ###
Another way is using pip

```
pip install gi-ape

```

### setuptools ###


## Configuration ##


APE requires two files to be executed correctly, both placed in the working directory:

### config.json ###

The config.json and configures pathes and files required by different tools in the pipline. It is written in [.json](http://www.json.org/json-de.html) format and currently configures 5 parameters:

* **genome**: a string referring to the used species and genome version, e.g *"hg38"* or *"mm10"*
* **ref**: the path to the reference in .fasta format, e.g. *"/vol/home/reference/hg38.fa"*
* **dbsnp**: the path to the [dbSNP](https://www.ncbi.nlm.nih.gov/projects/SNP/) database in [bzip](http://www.htslib.org/download/) compressed .vcf format (the current version v150 for homo-sapiens is available for download [here](ftp://ftp.ncbi.nih.gov/snp/organisms/human_9606_b150_GRCh38p7/VCF/All_20170710.vcf.gz))
* **track_path**: the path to the directory, that contains file with informations about regions covered in .bed files, e.g. "/vol/home/tracks/".
* **raw_path**: the path to the directory that contains your raw data, e.g. /vol/projects/neuroblastoma/raw


config.json example:
```
{
    "genome": "hg19",
    "ref": "/vol/home/ref/homo-sapiens/reference/human_g1k_v37.fasta",
    "dbsnp": "/vol/home/ref/homo-sapiens/dbsnp/dbsnp147.vcf.gz",
    "track_path": "/vol/home/homo-sapiens/tracks/",
    "raw_path": "/vol/projects/neuroblastoma/raw"
}

```

### units.txt ###

The units.txt configures the data processing samples itself. Each line of the file corresponds to exactly one sample and consists of 6 columns.

1. **sample name**: a unique identifier for each sample, e.g. *MySample1*. The generated file prefix will be prefixed with this id, e.g. *MySample1.bam*

2. **disease status**: an identifier of the sample condition, e.g. *Healty*, *Tumor* or *Blood*. This identifier is only relevant for the use with EAGLE, since samples will be automatically added to groups. Please note that for the use with eagle, the term *Healthy* is used for samples of the control group per default.

3. **forward path**: the path to the .fastq.gz file(s) for the forward reads. The path is given relative to the *raw_path* in config.json, e.g. *nature/13264N.R1.fastq.gz* (which gets transformed to */vol/projects/neuroblastoma/raw/nature/13264N.R1.fastq.gz* by the use of the example config.json. Regex for full Linux filesystem paths (^(/[^/ ]*)+/?$) may be used for samples with multiple input files, e.g. nature/13264N.*.R1.fastq.gz

4. **reverse path**: the path to the .fastq.gz file(s), similar to 3.

5. **capture kit**: the used capture kit file with captured regions, e.g. *agilent-sureselect.bed*. This value may be none if no capture-kit is available, but all calculated corresponding statistics will be 0 for this sample. The file is searched in the track-path provided by the config.json.

6. **calling group**: an identifier for the calling group. The snp caller will call all sample within the same group together, resulting in much more accurate calls. Please note that it is a good idea to put as many samples as possible in one calling group, but adding additional samples in an already processed group leads to a complete recall. Strong related samples, such as tumor and blood or father, mother index should always be contained in the same group.

units.txt example:
```
13264N	Healthy	nature/13264N.R1.fastq.gz	nature/13264N.R2.fastq.gz	none	13264
13264R	Relapse	nature/13264R.R1.fastq.gz	nature/13264R.R2.fastq.gz	none	13264
13264T	Tumor	nature/13264T.R1.fastq.gz	nature/13264T.R2.fastq.gz	none	13264
15800N	Healthy	nature/15800N.R1.fastq.gz	nature/15800N.R2.fastq.gz	none	15800
15800R	Tumor	nature/15800R.R1.fastq.gz	nature/15800R.R2.fastq.gz	none	15800
15800T	Tumor	nature/15800T.R1.fastq.gz	nature/15800T.R2.fastq.gz	none	15800
17041N	Healthy	nature/17041N.R1.fastq.gz	nature/17041N.R2.fastq.gz	none	17041
17041R1	Relapse	nature/17041R1.R1.fastq.gz	nature/17041R1.R2.fastq.gz	none	17041
17041R2	Relapse	nature/17041R2.R1.fastq.gz	nature/17041R2.R2.fastq.gz	none	17041
17041R3	Relapse	nature/17041R3.R1.fastq.gz	nature/17041R3.R2.fastq.gz	none	17041
17041R4	Relapse	nature/17041R4.R1.fastq.gz	nature/17041R4.R2.fastq.gz	none	17041
17041R5	Relapse	nature/17041R5.R1.fastq.gz	nature/17041R5.R2.fastq.gz	none	17041
```

This sample consists of 12 samples (one each row). Taking the example config.json into account, the first sample

* is named *13264N*
* has the status *Healthy*
* the forward reads are stored in /vol/projects/neuroblastoma/raw/nature/13264N.R1.fastq.gz
* the reverse reads are stored in /vol/projects/neuroblastoma/raw/nature/13264N.R2.fastq.gz
* on unknown capturekit was used
* should be called together with 13264R and 13264T


## Execution ##

The configured pipeline is started by calling

```
ape
```

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact