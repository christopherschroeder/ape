import h5py
import argparse


def main(args):

    outfile_name = args.output
    outfile = h5py.File(outfile_name, 'w')
    infile_names = args.input
    for infile_name in infile_names:
        infile = h5py.File(infile_name, 'r')
        for group in infile:
            if group not in outfile:
                infile.copy(group, outfile)
            else:
                for dataset in infile[group]:
                    infile[group].copy(dataset, outfile[group])
        infile.close()
    outfile.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input", nargs=2)
    parser.add_argument("output")
    args = parser.parse_args()

    main(args)