import vcf
from collections import namedtuple, defaultdict
from enum import IntEnum
import numpy as np
import h5py
import argparse
import sys


class Effect(IntEnum):
    none = 0,
    coding_sequence_variant = 1,
    chromosome = 2<<0,
    inframe_insertion = 2<<1,
    disruptive_inframe_insertion = 2<<2,
    inframe_deletion = 2<<3,
    disruptive_inframe_deletion = 2<<4,
    downstream_gene_variant = 2<<5,
    exon_variant = 2<<6,
    exon_loss_variant = 2<<7,
    frameshift_variant = 2<<8,
    gene_variant = 2<<9,
    intergenic_region = 2<<10,
    conserved_intergenic_variant = 2<<11,
    intragenic_variant = 2<<12,
    intron_variant = 2<<13,
    conserved_intron_variant = 2<<14,
    miRNA = 2<<15,
    missense_variant = 2<<16,
    initiator_codon_variant = 2<<17,
    non_coding_exon_variant = 2<<18,
    rare_amino_acid_variant = 2<<19,
    splice_acceptor_variant = 2<<20,
    splice_donor_variant = 2<<21,
    splice_region_variant = 2<<22,
    stop_lost = 2<<23,
    five_prime_UTR_premature = 2<<24,
    start_codon_gain_variant = 2<<25,
    start_lost = 2<<26,
    stop_gained = 2<<27,
    synonymous_variant = 2<<28,
    start_retained = 2<<29,
    stop_retained_variant = 2<<30,
    non_canonical_start_codon = 2<<31,
    five_prime_UTR_truncation = 2<<32,
    chromosome_number_variation = 2<<33,
    transcript = 2<<34,
    regulatory_region_variant = 2<<35,
    upstream_gene_variant = 2<<36,
    three_prime_UTR_variant = 2<<37,
    three_prime_UTR_truncation_and_exon_loss = 2<<38,
    five_prime_UTR_variant = 2<<39,
    five_prime_UTR_truncation_and_exon_loss_variant = 2<<40,
    five_prime_UTR_premature_start_codon_gain_variant = 2<<41,
    sequence_feature_and_exon_loss_variant = 2<<42,
    three_prime_UTR_truncation = 2<<43,
    exon_loss = 2<<44,
    transcript_ablation = 2<<45,
    non_coding_transcript_variant = 2<<46,
    protein_protein_contact = 2<<47,


def store(stored, out_filename):
    out_file = h5py.File(out_filename, 'w')
    for chrom in sorted(stored.keys()):
        grp_chrom = out_file.create_group(chrom)
        dtype = {'names':['start', 'end', 'typ' , 'het', 'length', 'qual', 'mq', 'effect', 'gene_id', 'rank_affected', 'rank_total', 'ref', 'coding', 'feature_id', 'hgvsc', 'hgvsp', 'distance', 'depth', 'impact'],
                'formats':['uint64', 'uint64', 'uint8', 'bool_', 'uint32', 'float32', 'float32', 'int64', 'a20', 'int8', 'int8', 'uint8', 'bool_', 'a20', 'a20', 'a20', 'int32', 'int32', "uint8"]}
        stored_np = np.array(np.array(stored[chrom], dtype=dtype))
        grp_chrom.create_dataset("structural", data=stored_np)


Annotation = namedtuple("Annotation", "allele effect impact gene geneid featuretype featureid biotype rank hgvsc hgvsp c_pos_len cds_pos protein_pos_len distance errors")


def main(args):

    stored = defaultdict(list)
    out_filename = args.output
    samplename = args.sample

    #base_to_index = { "A": 0, "C": 1, "G": 2, "T": 3 }

    for input_file in args.input:
        try:
            f = vcf.Reader(filename=input_file)
            for i, line in enumerate(f):
                genotype = line.genotype(samplename)
                if not genotype.is_variant: #ignore non variants sample
                    continue

                data = genotype.data

                #print(line.CHROM, line.POS, data.QR, data.QA, data.RO, data.AO, data, line.INFO["MQM"]*sum(line.INFO["AO"]), line.INFO["RO"], )


                chrom  = line.CHROM.upper()
                ref    = line.REF
                start    = line.POS
                end = line.INFO["END"]
                variant = str(line.ALT[0])
                length = line.INFO["INSLEN"] if variant == "<INS>" else end - start + 1
                annotations = line.INFO["ANN"]

                qual  = float(line.QUAL) if line.QUAL else 0
                #qd    = qual / sum([s.data.RC for s in line.samples if s.is_variant])
                mq    = line.INFO["MAPQ"]
                #freq  = sum([s.data.DV for s in line.samples if s.is_variant])/sum([s.data.RR + s.data.DV for s in line.samples if s.is_variant])
                het   = genotype.is_het
                depth = genotype.data.RC

                for a in annotations:
                    A = Annotation(*a.split("|"))
                    coding = A.biotype == "Coding" # maybe there are other biotypes
                    distance = A.distance if len(A.distance) else -1

                    if A.rank:
                        rank_affected, rank_total = map(int, A.rank.split("/"))
                    else:
                        rank_affected, rank_total = 0, 0

                    e = 0
                    for effect in A.effect.split("&"):
                        if not effect:
                            effect = "none"
                        reffect_replaced = effect.replace("3_", "three_").replace("5_", "five_")
                        e = e | Effect[reffect_replaced]


                    if variant == "<DEL>": #DEL
                        typ = 5 # 101
                    elif variant == "<INS>": #INS
                        typ = 6 # 110
                    elif variant == "<INV>": #INV
                        typ = 7 # 111
                    ref_char = 0

                    impact = ["MODIFIER", "LOW", "MODERATE", "HIGH"].index(A.impact)

                    stored[chrom].append((start, end, typ, het, length, qual, mq, e, A.geneid, rank_affected, rank_total, ref_char, coding,  A.featureid, A.hgvsc, A.hgvsp, distance, depth, impact))
        except (StopIteration):
            pass
    store(stored, out_filename)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input", nargs='+')
    parser.add_argument("sample")
    parser.add_argument("output")
    args = parser.parse_args()

    main(args)

