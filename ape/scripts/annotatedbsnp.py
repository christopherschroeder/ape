import argparse
import h5py
import numpy as np
import sys
from numpy.lib.recfunctions import merge_arrays
import os

def main(args):
    f = h5py.File(args.input, "r+")
    f_dbs = [h5py.File(dbsnp, "r") for dbsnp in args.dbsnp]
    db_names = [os.path.basename(filename).rsplit(".", -1)[0] for filename in args.dbsnp]


    for chrom in set(f) & set(f_dbs[0]):
        print(chrom, file=sys.stderr)
        chrom_grp = f[chrom]
        if "db" in chrom_grp:
            del chrom_grp["db"]
        keys = chrom_grp["variants"]["key"][:] >> 1 #shift to ignore heterozygious status
        in_db = np.rec.fromarrays([np.in1d(keys, f_db[chrom]["variants"]["key"][:] >> 1) for f_db in f_dbs], names=db_names)
        chrom_grp.create_dataset("db", data=in_db, maxshape=in_db.shape)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input")
    parser.add_argument("dbsnp", nargs="+")
    args = parser.parse_args()

    main(args)
