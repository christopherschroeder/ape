import argparse
import gzip

def main(args):
    s = 0

    with gzip.open(args.input, 'rb') as f:
        for line in f:
            if not line.startswith(b"all"):
                continue

            split=line.split(b"\t")
            coverage = int(split[1])
            count    = int(split[2])

            s += coverage*count
    print(s)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input")
    args = parser.parse_args()

    main(args)
