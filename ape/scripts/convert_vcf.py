import vcf
from collections import namedtuple, defaultdict
from enum import IntEnum
import numpy as np
import h5py
import argparse
import sys


class Effect(IntEnum):
    none = 0,
    coding_sequence_variant = 1,
    chromosome = 2<<0,
    inframe_insertion = 2<<1,
    disruptive_inframe_insertion = 2<<2,
    inframe_deletion = 2<<3,
    disruptive_inframe_deletion = 2<<4,
    downstream_gene_variant = 2<<5,
    exon_variant = 2<<6,
    exon_loss_variant = 2<<7,
    frameshift_variant = 2<<8,
    gene_variant = 2<<9,
    intergenic_region = 2<<10,
    conserved_intergenic_variant = 2<<11,
    intragenic_variant = 2<<12,
    intron_variant = 2<<13,
    conserved_intron_variant = 2<<14,
    miRNA = 2<<15,
    missense_variant = 2<<16,
    initiator_codon_variant = 2<<17,
    non_coding_exon_variant = 2<<18,
    rare_amino_acid_variant = 2<<19,
    splice_acceptor_variant = 2<<20,
    splice_donor_variant = 2<<21,
    splice_region_variant = 2<<22,
    stop_lost = 2<<23,
    five_prime_UTR_premature = 2<<24,
    start_codon_gain_variant = 2<<25,
    start_lost = 2<<26,
    stop_gained = 2<<27,
    synonymous_variant = 2<<28,
    start_retained = 2<<29,
    stop_retained_variant = 2<<30,
    non_canonical_start_codon = 2<<31,
    protein_protein_contact = 2<<32,
    non_coding_transcript_variant = 2<<33,
    transcript = 2<<34,
    regulatory_region_variant = 2<<35,
    upstream_gene_variant = 2<<36,
    three_prime_UTR_variant = 2<<37,
    three_prime_UTR_truncation_and_exon_loss = 2<<38,
    five_prime_UTR_variant = 2<<39,
    five_prime_UTR_truncation_and_exon_loss_variant = 2<<40,
    five_prime_UTR_premature_start_codon_gain_variant = 2<<41,
    sequence_feature_and_exon_loss_variant = 2<<42
    TF_binding_site_variant = 2 << 43


def store(stored, samplenames, out_filenames):
    for samplename, out_filename in zip(samplenames, out_filenames):
        out_file = h5py.File(out_filename, 'w')
        for chrom in sorted(stored[samplename].keys()):
            grp_chrom = out_file.create_group(chrom)
            dtype = {'names':['key', 'length', 'qual', 'qd', 'mq', 'effect', 'gene_id', 'rank_affected', 'rank_total', 'ref', 'coding', 'feature_id', 'hgvsc', 'hgvsp', 'distance', 'alt_count', 'depth', 'context', 'impact', 'rsid', 'precious', 'common'],
                    'formats':['int64', 'uint32', 'float32', 'float32', 'float32', 'int64', 'a20', 'int8', 'int8', 'uint8', 'bool_', 'a20', 'a20', 'a20', 'int32', 'int32', 'int32', 'a3', 'uint8', 'uint32', 'bool_', 'bool_']}

            stored_np = np.array(np.array(stored[samplename][chrom], dtype=dtype))

            grp_chrom.create_dataset("variants", data=stored_np)
            keys = np.unique(stored_np["key"])
            grp_chrom.create_dataset("variant_keys", data=keys, dtype=np.int64)


Annotation = namedtuple("Annotation", "allele effect impact gene geneid featuretype featureid biotype rank hgvsc hgvsp c_pos_len cds_pos protein_pos_len distance errors")


def main(args):
    f = vcf.Reader(filename=args.input)

    stored = defaultdict(lambda: defaultdict(list))

    out_filenames = args.output
    samplenames = args.samples

    base_to_index = { "A": 0, "C": 1, "G": 2, "T": 3 }

    for i, line in enumerate(f):
        chrom = line.CHROM.upper()
        ref   = line.REF
        pos   = line.POS

        rsid = line.ID
        if not rsid: rsid = "rs0"
        rsid = int(rsid.split(";")[0][2:])
        pm = "PM" in line.INFO

        qual  = float(line.QUAL) if line.QUAL else 0
        dp = sum([s.data.DP for s in line.samples if s.is_variant])
        if dp == 0:
            continue
        qd    = qual / dp
        mq    = sum([i*j for i,j in zip(line.INFO["AO"], line.INFO["MQM"])]) / sum(line.INFO["AO"])

        if "N" in ref:
            continue

        for samplename in samplenames:
            genotype = line.genotype(samplename)
            if not genotype.is_variant: #ignore non variants sample
                continue

            data = genotype.data
            alt_c = genotype.data.AO
            het   = genotype.is_het
            depth = genotype.data.DP

            annotations = line.INFO["ANN"]

            context = line.INFO["CTX"][0] if "CTX" in line.INFO else ""

            ao = [genotype.data.AO] if type(genotype.data.AO) == int else genotype.data.AO

            if not type(alt_c) is list:
                alt_c = [alt_c]

            common = "COMMON" in line.INFO and line.INFO["COMMON"] == 1

            for alt, c in zip(map(str, line.ALT), alt_c):
                for a in annotations:
                    A = Annotation(*a.split("|"))
                    coding = A.biotype == "Coding" # maybe there are other biotypes
                    distance = A.distance if len(A.distance) else -1

                    if A.rank:
                        rank_affected, rank_total = map(int, A.rank.split("/"))
                    else:
                        rank_affected, rank_total = 0, 0

                    e = 0
                    for effect in A.effect.split("&"):
                        if not effect:
                            effect = "none"
                        reffect_replaced = effect.replace("3_", "three_").replace("5_", "five_")
                        e = e | Effect[reffect_replaced]

                    if len(alt) == 1 and len(ref) == 1: # snp
                        typ = base_to_index[alt] # 000, 001, 010, 011
                        ref_char = ord(line.REF)
                    else: #structural variant
                        if len(alt) == len(ref): # mnp
                            typ = 4 # 100
                        elif len(alt) > 1 and len(ref) == 1: #ins
                            typ = 5 # 101
                        elif len(alt) == 1 and len(ref) > 1: #del
                            typ = 6 # 110
                        else: # something else
                            typ = 7 # 110
                        ref_char = 0

                    key = (pos << 4) + (typ << 1) + het

                    length = max(len(alt), len(ref))

                    impact = ["MODIFIER", "LOW", "MODERATE", "HIGH"].index(A.impact)

                    stored[samplename][chrom].append((key, length, qual, qd, mq, e, A.geneid, rank_affected, rank_total, ref_char, coding, A.featureid, A.hgvsc, A.hgvsp, distance, c, depth, context, impact, rsid, pm, common))

    #TODO: svm_D additional to impact!

    store(stored, samplenames, out_filenames)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="the input in vcf format")
    parser.add_argument("--samples", nargs="*", help="the samples to extract from the file")
    parser.add_argument("--output", nargs="*", help="the ouput h5 format for eagle")
    args = parser.parse_args()

    main(args)

