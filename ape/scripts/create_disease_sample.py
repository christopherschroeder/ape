import argparse
import h5py
import os
import numpy as np

def run(output, samples):
    o = h5py.File(output, "w")
    #write sample names
    base_samples = [os.path.splitext(os.path.basename(s))[0] for s in samples]
    o.create_dataset("samples", data=np.array(base_samples, dtype="a"))

    #write snps
    grp = o.create_group("variants")

    fs = [h5py.File(s) for s in samples]

    for chrom in fs[0]["variants"]:
        chrom_grp = grp.create_group(chrom)
        values = [f["variants"][chrom]["values"]["key"][:] for f in fs if chrom in f["variants"] and f["variants"][chrom]["values"]["key"].shape[0] > 0]
        unique, counts = np.unique(np.concatenate(values), return_counts=True)
        chrom_grp.create_dataset("values", data=np.asarray((unique, counts)).T)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("output")
    parser.add_argument("samples", nargs="+")
    args = parser.parse_args()
    run(args.output, args.samples)


if __name__ == "__main__":
    main()
