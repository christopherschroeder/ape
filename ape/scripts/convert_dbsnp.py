import vcf
from collections import namedtuple, defaultdict
from enum import IntEnum
import numpy as np
import h5py
import argparse
import sys

from itertools import islice


def store(stored, out_filename):
    out_file = h5py.File(out_filename, 'w')
    for chrom in sorted(stored.keys()):
        grp_chrom = out_file.create_group(chrom)
        dtype = {'names':['key', 'length', 'id'],
                'formats':['uint64', 'uint32', 'a11']}

        stored_np = np.array(np.array(stored[chrom], dtype=dtype))
        grp_chrom.create_dataset("variants", data=stored_np)


def main(args):
    f = vcf.Reader(filename=args.input)

    stored = defaultdict(list)

    out_filename = args.output


    base_to_index = { "A": 0, "C": 1, "G": 2, "T": 3 }

    for i, line in enumerate(f):
        if args.noprecious and "PM" in line.INFO: # ignore precious, if parameter is set
            continue

        chrom = line.CHROM.upper()
        
        #unify chromosome names
        if not chrom.startswith('CHR'):
            if chrom == "MT":
                chrom = "CHRM"
            else:
                chrom = "CHR"+chrom

        ref = line.REF
        pos = line.POS

        id  = line.ID

        for alt in map(str, line.ALT):
            if len(alt) == 1 and len(ref) == 1: # snp
                if alt not in base_to_index:
                    continue
                typ = base_to_index[alt] # 000, 001, 010, 011
                ref_char = ord(line.REF)
            else: #structural variant
                if len(alt) == len(ref): # mnp
                    typ = 4 # 100
                elif len(alt) > 1 and len(ref) == 1: #ins
                    typ = 5 # 101
                elif len(alt) == 1 and len(ref) > 1: #del
                    typ = 6 # 110
                else: # something else
                    typ = 7 # 110
                ref_char = 0

            key = (pos << 4) + (typ << 1) + 0 #homozygious

            length = max(len(alt), len(ref))
            stored[chrom].append((key, length, id))


    store(stored, out_filename)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input")
    parser.add_argument("output")
    parser.add_argument("--noprecious", action="store_true", help="ignore precious variants")
    args = parser.parse_args()

    main(args)

