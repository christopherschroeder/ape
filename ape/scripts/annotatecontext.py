import sys
from vcf import Reader, Writer
from vcf.parser import _Info as VcfInfo

from sqt.io.fasta import IndexedFasta
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("vcf")
parser.add_argument("ref")
args=parser.parse_args()


f = Reader(open(args.vcf))
r = IndexedFasta(args.ref)

#add CTX field to header
f.infos["CTX"] = VcfInfo(id='CTX', num="A", type='String', desc="Context of the Variant", source="ape", version="1.0")

writer = Writer(sys.stdout, f)

for i, item in enumerate(f):
    if max([len(x) for x in item.ALT]) == 1 and len(item.REF) == 1:
        x = r.get(item.CHROM)[item.POS-2:item.POS+1].decode()
        item.INFO["CTX"] = x
    writer.write_record(item)
